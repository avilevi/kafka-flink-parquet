package org.example

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.flink.streaming.connectors.fs.Writer
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.parquet.avro.AvroParquetWriter
import org.apache.parquet.hadoop.metadata.CompressionCodecName
import org.apache.parquet.hadoop.{ParquetFileWriter, ParquetWriter}
import org.slf4j.LoggerFactory

import scala.util.Try

class ParquetSinkWriter[GenericRecord](pageSize: Int = ParquetWriter.DEFAULT_PAGE_SIZE
                          ) extends Writer[GenericRecord] {

  val log = LoggerFactory.getLogger(this.getClass)

  @transient
  val schemaString: String =
    """{"type":"record","name":"request","namespace":"request.schema",
      |"fields":[
      |{"name":"qname","type":"string"},
      |{"name":"rcode","type":"string"},
      |{"name":"ts","type":"long"}]}""".stripMargin

  @transient
  val schema: Schema = new Schema.Parser().parse(schemaString)

  @transient
  private var _path: Path = _

 @transient
  private val compressionCodecName = CompressionCodecName.SNAPPY

  @transient
  private var position: Long = 0

  @transient
  private var writer: Option[ParquetWriter[GenericRecord]] = None

  @transient
  private def getWriter(path: Path): Option[ParquetWriter[GenericRecord]] = {
    Option(AvroParquetWriter.builder[GenericRecord](path)
      .withSchema(schema)
      .withCompressionCodec(compressionCodecName)
      .withPageSize(pageSize)
      .withWriteMode(ParquetFileWriter.Mode.OVERWRITE)
      .build())
  }

  override def open(fs: FileSystem, path: Path): Unit = {
    log.debug(s"Opening $path")
    _path = path
    position = 0
    writer.foreach(_.close)
    writer = getWriter(path)

  }

  override def flush(): Long = {
    position += writer.map(_.getDataSize).getOrElse(0L)
    log.debug(s"Flushing $position data size ${ writer.map(_.getDataSize)}")
    writer.foreach(_.close)
    writer = getWriter(_path)
    position

  }

  override def getPos: Long = {
    log.debug(s"Get pos ${ writer.map(_.getDataSize)}, position: $position")
    position + writer.map(_.getDataSize).getOrElse(0L)
  }

  override def close(): Unit = {
    log.debug(s"Closing $position")
      writer.foreach(_.close)
      writer = None
  }

  override def write(element: GenericRecord): Unit = {
    if (writer.isEmpty)
      writer = getWriter(_path)
    log.debug(s"writing pos ${ writer.map(_.getDataSize)}, position: $position writing $element")

    writer.foreach(_.write(element))
  }

  override def duplicate(): Writer[GenericRecord] = {
    new ParquetSinkWriter()
  }
}
