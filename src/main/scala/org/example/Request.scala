package org.example

import java.nio.charset.StandardCharsets

import org.apache.flink.api.common.serialization.{DeserializationSchema, SerializationSchema}
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import spray.json.{DefaultJsonProtocol, RootJsonFormat, _}

case class Request (qname: String, rcode: String, ts:Long)
object Request extends DefaultJsonProtocol {
  implicit val requestFormat: RootJsonFormat[Request] = jsonFormat3(Request.apply)
}

class RequestSchema extends DeserializationSchema[Request] with SerializationSchema[Request] {
  import Request._
  @transient lazy val log = LoggerFactory.getLogger(this.getClass)

  private val mapper: ObjectMapper = new ObjectMapper()

  implicit val typeInfo = TypeInformation.of(classOf[Request])

  override def deserialize(bytes: Array[Byte]): Request = {
      try{
        new String(bytes, StandardCharsets.UTF_8).parseJson.convertTo[Request]
      } catch{
        case err =>
          log.error(s"Fail to desirialize ${bytes.map(_.toChar).mkString}", err)
          Request("Failed parsing", "Parse Error", 0L)
      }
  }

  override def isEndOfStream(t: Request): Boolean = false

  override def serialize(t: Request): Array[Byte] = t.toJson.toString().getBytes(StandardCharsets.UTF_8)

  override def getProducedType: TypeInformation[Request] = TypeInformation.of(classOf[Request])
}
