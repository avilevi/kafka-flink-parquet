package org.example

/**
  * Licensed to the Apache Software Foundation (ASF) under one
  * or more contributor license agreements.  See the NOTICE file
  * distributed with this work for additional information
  * regarding copyright ownership.  The ASF licenses this file
  * to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import java.util.Properties

import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.CheckpointingMode
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.fs.bucketing.{BucketingSink, DateTimeBucketer}
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011

import scala.io.Source

object StreamingJob extends App {
  val env = StreamExecutionEnvironment.getExecutionEnvironment
  val Topic = "first_seen"

  val consumerProperties = new Properties()
  consumerProperties.setProperty("bootstrap.servers", "localhost:9092")
  consumerProperties.setProperty("group.id", "bv.fsdomains")
  consumerProperties.setProperty("enable.auto.commit", "true")
  consumerProperties.setProperty("auto.commit.interval.ms", "100")

  val consumer = new FlinkKafkaConsumer011[Request](Topic, new RequestSchema(), consumerProperties)
  env.enableCheckpointing(1000, CheckpointingMode.EXACTLY_ONCE)
  env.setParallelism(1)
  val stream = env.addSource(consumer)
//  val stream = env.fromElements(Request("aaa", "nothing", 1L), Request("bbb", "na", 2))
  val writer = new ParquetSinkWriter[GenericRecord]
  val bucketingSink = new BucketingSink[GenericRecord]("/Users/avilevi/tmp/streaming2/")
  bucketingSink.setBucketer(new DateTimeBucketer[GenericRecord]("yyyy-MM-dd--HHmm"))
  bucketingSink.setWriter(writer)
//  parquetBucketingSink.setBatchSize(1024 * 1024 * 400) // this is 400 MB,
  bucketingSink.setBatchRolloverInterval(1 * 30 * 1000); // this is 1 mins

  val schemaString: String =
  """{"type":"record","name":"request","namespace":"request.schema",
    |"fields":[
    |{"name":"qname","type":"string"},
    |{"name":"rcode","type":"string"},
    |{"name":"ts","type":"long"}]}""".stripMargin

  val schema: Schema = new Schema.Parser().parse(schemaString)
  stream.map{ r=>
    val genericReocrd: GenericRecord = new GenericData.Record(schema)
    genericReocrd.put("qname", r.qname)
    genericReocrd.put("rcode", r.rcode)
    genericReocrd.put("ts", r.ts)
    genericReocrd

  }.addSink (bucketingSink)
  env.execute()
}
