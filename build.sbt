ThisBuild / resolvers ++= Seq(
  "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
  Resolver.mavenLocal
)

name := "kafka-flink-parquet"

version := "0.1-SNAPSHOT"

organization := "com.bluevoyant"

val mainV = "0.0.1"

ThisBuild / scalaVersion := "2.11.12"

assemblyMergeStrategy in assembly := {
  case "META-INF/io.netty.versions.properties" => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

val flinkV            = "1.6.2"
val kafkaV            = "0.11.0.1"
val logbackV          = "1.2.2"
val slf4jV            = "1.7.25"
val sprayV            = "1.3.5"

val scalaTestV        = "3.2.0-SNAP10"
val scalaMockV        = "3.6.0"
val embeddedKafkaV    = "2.0.0"
val scalaCheckV       = "1.14.0"

val flinkScala      = "org.apache.flink"        %% "flink-scala"                        % flinkV
val flinkStreaming  = "org.apache.flink"        %% "flink-streaming-scala"              % flinkV
val flinkkafka      = "org.apache.flink"        %% "flink-connector-kafka-0.11"         % flinkV
val flinkRocksDb    = "org.apache.flink"        %% "flink-statebackend-rocksdb"         % flinkV
val flinkAvro       = "org.apache.flink"        %  "flink-avro"                         % flinkV
val flinkParquet    = "org.apache.flink"        %  "flink-parquet"                      % flinkV
val flinkFs         = "org.apache.flink"        %%  "flink-connector-filesystem"        % flinkV

val parquetAvro     = "org.apache.parquet"      %  "parquet-avro"                       % "1.10.0"
val hadoopClient    = "org.apache.hadoop"       %  "hadoop-client"                      % "2.8.0"
val sprayJson       = "io.spray"                %% "spray-json"                         % sprayV
//val logback         = "ch.qos.logback"          %   "logback-classic"                   % logbackV

val eels            = "io.eels" %% "eel-parquet" % "0.19.1"
val eelsComponent   = "io.eels" %% "eel-components" % "1.1.1"
val logSlf4j        = "org.slf4j"               % "slf4j-simple"                        % slf4jV
val log12           = "org.slf4j"               % "slf4j-log4j12"                       % slf4jV
val logjdk          = "org.slf4j"               % "slf4j-jdk14"                         % slf4jV


val scalaTest       = "org.scalatest"           %% "scalatest"                          % scalaTestV      % Test
val scalaMock       = "org.scalamock"           %% "scalamock-scalatest-support"        % scalaMockV      % Test
val embeddedKafkaTest = "net.manub"             %% "scalatest-embedded-kafka"           % embeddedKafkaV  % Test //exclude("org.apache.kafka", "kafka_2.11")
val scalaCheck       = "org.scalacheck"         %% "scalacheck"                         % scalaCheckV     % Test
val flinkTest        = "org.apache.flink"       %% "flink-test-utils"                   % flinkV          % Test

val parquetDependencies = Seq(
  eels,
  eelsComponent,
  flinkAvro,
  flinkParquet,
  parquetAvro,
  hadoopClient
)


val commonDependencies = Seq(
  flinkFs,
  flinkScala,
  flinkStreaming,
  flinkkafka,
  flinkRocksDb,
  sprayJson     )

val testingDependencies = Seq( scalaTest,
  flinkTest,
  scalaMock,
  scalaCheck,
  embeddedKafkaTest)

lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= parquetDependencies ++ commonDependencies ++ testingDependencies
  )

assembly / mainClass := Some("org.example.StreamingJob")

// make run command include the provided dependencies
Compile / run  := Defaults.runTask(Compile / fullClasspath,
  Compile / run / mainClass,
  Compile / run / runner
).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption  := (assembly / assemblyOption).value.copy(includeScala = false)